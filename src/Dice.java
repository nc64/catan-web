
public class Dice {
        
            private int dice1,dice2;   
            
            public Dice() {
                    // constructor to roll dice so they show rand values
                roll();  
            }
            
            public void roll() {
                    // roll the dice 
                dice1 = (int)(Math.random()*6) + 1;
                dice2 = (int)(Math.random()*6) + 1;
            }
            
            public int getTotal() {
                // Return the total showing on the two dice.
             return dice1 + dice2;
          }
            
        } 