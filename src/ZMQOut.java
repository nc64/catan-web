import java.io.IOException;

import org.zeromq.ZMQ;

import com.google.protobuf.InvalidProtocolBufferException;

import bleat.RequestOuterClass;
import bleat.RequestOuterClass.Request;
import intergroup.Events;
import intergroup.Requests;
import intergroup.board.Board.Edge;

public class ZMQOut {
    ZMQ.Socket socket;
    
    
    public ZMQOut(){
        ZMQ.Context context = ZMQ.context(1); 
        socket = context.socket(ZMQ.REQ);
        socket.connect ("tcp://localhost:" + 4242);
    }
    
    public void parseMsg(intergroup.Messages.Message msg){
        switch(msg.getTypeCase()){
        case EVENT:
        	Events.Event event = msg.getEvent();
//        	switch(event.getTypeCase()){
//        	case ROADBUILT:
//        		Edge target1 = event.getRoadBuilt();
//        		RequestOuterClass.Request roadBuildReq = buildRoad(target1);
//                sendRequest(roadBuildReq);
//        		break;
//        	case SETTLEMENTBUILT:
//        		intergroup.board.Board.Point target2 = event.getSettlementBuilt();
//        		RequestOuterClass.Request settlementBuildReq = buildSettlement(target2);
//                sendRequest(settlementBuildReq);
//        		break;
//        	case CITYBUILT:
//        		intergroup.board.Board.Point target3 = event.getCityBuilt();
//        		RequestOuterClass.Request cityBuildReq = buildSettlement(target3);
//                sendRequest(cityBuildReq);
//        		break;
//        	case TURNENDED:
//        		RequestOuterClass.Request turnEndedReq = buildEndTurn();
//                sendRequest(turnEndedReq);
//        		break;
//        	case ROLLED:
//        		RequestOuterClass.Request diceRollReq = buildDiceRoll();
//                sendRequest(diceRollReq);
//        		break;
//        	}
        	break;
        case REQUEST:
        	Requests.Request request = msg.getRequest();
//        	switch(request.getTypeCase()){
//        	
//        	}
            break;
        }
    }
    
    public boolean sendRequest(RequestOuterClass.Request request){
    	System.out.println("ZMQOut: request sent");
       // System.out.println(request);
        socket.sendMore(RequestOuterClass.Request.getDescriptor().getFullName());
        socket.send(request.toByteArray(), 0);
        
        try {
            String responseType = socket.recvStr();
            if(!socket.hasReceiveMore()){
                System.err.println("Invalid packet received");
            }
            
            byte[] reply = socket.recv();
            bleat.ResponseOuterClass.Response response = bleat.ResponseOuterClass.Response.parseFrom(reply);
            System.out.println("Move was valid: " + response.getSuccess());
//            return response.getSuccess();
            return true;
        } catch ( InvalidProtocolBufferException x){
            System.err.println(x);
        }
        
        return false;
    }
}
