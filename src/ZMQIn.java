import com.google.protobuf.Empty;
import com.google.protobuf.InvalidProtocolBufferException;

import CatanMain.CoordRotation;
import intergroup.board.Board.Edge;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.zeromq.ZMQ;
import bleat.*;
import intergroup.*;
import intergroup.Requests.Request;

public class ZMQIn extends Thread{

OutputStream os;
intergroup.Messages.Message msg = null;
InputStream is;
int socket;
   
@Override
public void run() {
	
    ZMQ.Context context = ZMQ.context(1);
    ZMQ.Socket subscriber = context.socket(ZMQ.SUB);
    byte[] filter = {};
    subscriber.subscribe(filter);
    subscriber.connect("ipc:///tmp/catan_events");
    System.out.println("ZMQIn: listening for bleat events");
    

    
    
    while (!Thread.currentThread ().isInterrupted ()) {
        // Read envelope with address
        byte[] address = subscriber.recv();
        // Read message contents
        byte[] contents = subscriber.recv();
        System.out.print("ZMQIn: "+new String(address) + " : \n");
        
        try {
            bleat.BleatEvents.BleatEvent event = bleat.BleatEvents.BleatEvent.parseFrom(contents);
            //System.out.print(event.toString());

            switch(event.getTypeCase()){
            case ON_CITY_BUILT:  
            	// front end says city has been built so send req to server to build city
                intergroup.board.Board.Point target1 = event.getOnCityBuilt().getPoint();
                intergroup.Requests.Request cityReq = buildCity(target1);
                intergroup.Messages.Message.newBuilder().setRequest(cityReq).build();
                System.out.println("ZQMIn: cityReqMsg received \n");
                break;
            case ON_ROAD_BUILT:
            	intergroup.board.Board.Edge edge = ProtocolBuilder.edgeFromRoadBuild(event.getOnRoadBuilt());
                intergroup.Requests.Request roadReq = buildRoad(edge);
                intergroup.Messages.Message.newBuilder().setRequest(roadReq).build();
                System.out.println("ZQMIn: roadReqMsg received");
                break;
            case ON_SETTLEMENT_BUILT:
                intergroup.board.Board.Point target2 = event.getOnSettlementBuilt().getPoint();
                intergroup.Requests.Request settlementReq = buildSettlement(target2);
                intergroup.Messages.Message.newBuilder().setRequest(settlementReq).build();
                System.out.println("ZQMIn: settlementReqMsg received");
                break;
            case ON_CURRENT_PLAYER_CHANGED:
                int player = event.getOnCurrentPlayerChanged().getPlayer().getId().getNumber();
                intergroup.EmptyOuterClass.Empty empty = intergroup.EmptyOuterClass.Empty.newBuilder().build();
                intergroup.Requests.Request endTurnReq = endTurn(empty);
                intergroup.Messages.Message.newBuilder().setRequest(endTurnReq).build();
                System.out.println("ZQMIn: endTurnReqMsg received");
                break;
            case ON_PLAYER_RESOURCES:
            	System.out.println("ZQMIn: no action needed");
                break;
            case ON_VICTORY_POINTS_CHANGED:
            	System.out.println("ZQMIn: no action needed");
                break;
            case ON_DICE_ROLLED:
                intergroup.EmptyOuterClass.Empty empty2 = intergroup.EmptyOuterClass.Empty.newBuilder().build();
                intergroup.Requests.Request rollDiceReq = rollDice(empty2);
                intergroup.Messages.Message.newBuilder().setRequest(rollDiceReq).build();
                System.out.println("diceRollReqMsg recieved");
                break;
            case ON_GAME_PHASE_CHANGED:
            	System.out.println("ZQMIn: no action needed");
            	break;
            case ON_GAME_STARTED:
            	System.out.println("ZQMIn: GameStartedEvent sent");
            	intergroup.lobby.Lobby.GameSetup setup = event.getOnGameStarted().getGameSetup();
            	intergroup.Events.Event startEvent = intergroup.Events.Event.newBuilder().setBeginGame(setup).build();
            	intergroup.Messages.Message msg1 = intergroup.Messages.Message.newBuilder().setEvent(startEvent).build();
//            	msg1.writeDelimitedTo(os);
            	setMsg(msg1);
            	break;
            	
            default: System.out.println("ZMQIn default case");
}

        } catch ( InvalidProtocolBufferException x){
           System.err.println(x);
        } catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
    }
    
	context.term();
	subscriber.close();
    
}

public void setOs(OutputStream os) {
	this.os = os;
} 

public void setMsg(intergroup.Messages.Message msg) {
	this.msg = msg;
} 

public intergroup.Messages.Message getMsg() {
	return msg;
} 

public static intergroup.Requests.Request buildSettlement(intergroup.board.Board.Point target) {
    return intergroup.Requests.Request.newBuilder()
            .setBuildSettlement(target)
            .build();
}

public static intergroup.Requests.Request buildRoad(intergroup.board.Board.Edge edge) {
    return intergroup.Requests.Request.newBuilder()
            .setBuildRoad(edge)
            .build();
}

public static intergroup.Requests.Request buildCity(intergroup.board.Board.Point target){
    return intergroup.Requests.Request.newBuilder()
            .setBuildCity(target)
            .build();
}

public static intergroup.Requests.Request endTurn(intergroup.EmptyOuterClass.Empty empty){
    return intergroup.Requests.Request.newBuilder()
            .setEndTurn(empty)
            .build();
}

public static intergroup.Requests.Request rollDice(intergroup.EmptyOuterClass.Empty empty){
    return intergroup.Requests.Request.newBuilder()
            .setRollDice(empty)
            .build();
}

}


    