
import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;

import org.zeromq.ZMQ;

import com.google.protobuf.InvalidProtocolBufferException;

import bleat.RequestOuterClass;
import bleat.RequestOuterClass.Request;
import intergroup.board.Board;
import intergroup.lobby.Lobby.GameSetup;
import intergroup.lobby.Lobby.GameSetup.PlayerSetting.Colour;
import intergroup.lobby.Lobby.Join;

//basic multi-threaded server
public class Server{


	// The server socket.
	private static ServerSocket serverSocket = null;
	private static int ZMQport = 4242;
	// The client socket.
	private static Socket clientSocket = null;

	private static final int maxClientsCount = 4;
	private static final clientThread[] threads = new clientThread[maxClientsCount];
	private static final ArrayList<GameSetup.PlayerSetting> players = new ArrayList<GameSetup.PlayerSetting>();
	private static InputStream serverIs;
	static ZMQ.Socket socket;
	static ZMQ.Socket subscriber;

	static ZMQIn zIn;
	static ZMQOut zOut;
	public static void start() throws UnknownHostException {

		// start the proxy to translate between protocols
		zIn= new ZMQIn();
		new Thread(zIn).start();
		zOut = new ZMQOut();		



		// the default port number.
		int portNumber = 2222;
		InetAddress ip = InetAddress.getByName("127.0.0.1");
				
		System.out.println("Catan Server\n"+ "Now using port number=" + portNumber);

		// open server socket
		try {
			// can specify IP address for server later - for now stick with default localhost
			serverSocket = new ServerSocket(portNumber,10,ip);
		} catch (IOException e) {
			System.out.println(e);
		}

		// create client socket and pass to client thread
		while (true) {
			try {
				clientSocket = serverSocket.accept();
				int i = 0;
				for (i = 0; i < maxClientsCount; i++) {
					if (threads[i] == null) {
						(threads[i] = new clientThread(clientSocket, threads, players, i, zIn, zOut)).start();
						break;
					}
				}
				// if 4 players are already in lobby reject connection
				if (i == maxClientsCount) {
					PrintStream os = new PrintStream(clientSocket.getOutputStream());
					os.println("Lobby is full");
					os.close();
					clientSocket.close();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	}
}

// client threads opens i/o streams and when data is received echos this data to other clients
class clientThread extends Thread {

	public int id;

	private InputStream serverIs = null;
	private OutputStream os = null;
	private InputStream is = null;
	private Socket clientSocket = null;
	private final clientThread[] threads;
	private int maxClientsCount;
	private boolean joined;
	private String username = null;
	private Board.Player player;
	private ArrayList<GameSetup.PlayerSetting> players = new ArrayList<GameSetup.PlayerSetting>();
	static ZMQIn zIn;
	static ZMQOut zOut;

	public clientThread(Socket clientSocket, clientThread[] threads, ArrayList<GameSetup.PlayerSetting> players, int id, ZMQIn zIn, ZMQOut zOut) {
		this.clientSocket = clientSocket;
		this.threads = threads;
		this.players = players;
		this.zIn = zIn;
		this.zOut = zOut;
		this.joined = false;
		// will make sure id is unique later
		this.id = id+1;
		maxClientsCount = threads.length;

	}

	public int countClients(clientThread[] threads){
		int count = 0;
		for(int i = 0; i < maxClientsCount; i++){
			if(threads[i] != null){
				count++;
			}
		}
		return count;
	}

	public void run() {
		




		int maxClientsCount = this.maxClientsCount;
		clientThread[] threads = this.threads;
		ArrayList<GameSetup.PlayerSetting> players = this.players;
		String name = ("Player "+ this.id);
		//System.out.println(threads.);
		try {
			// create input and output streams for this client.

			is = clientSocket.getInputStream();
			os = clientSocket.getOutputStream();
			zIn.setOs(os);
			// ** get name and colour at some point ** 
			// stay in this loop until connected client has joined

			int count = countClients(threads);
			System.out.println(count);

			// wait for individual client to join
			while(joined == false){
				intergroup.Messages.Message msg = intergroup.Messages.Message.parseDelimitedFrom(is);
				Join join = Parser.parseJoinMsg(msg);
				//System.out.println(msg.toString());
				if(join != null){
					String username = join.getUsername();
					//				 this.username = name;
					this.username = username;
					this.joined = true;
				}
			}
			System.out.println("("+this.id+") "+username + " connected");
			//			}
			boolean allClientsJoined = false;
			
			// wait until clients have joined
			while(count < maxClientsCount){
				//System.out.println("Waiting for " + count + "more players to connect");
			}
			
			while(!allClientsJoined){
				for (int i = 0; i < count; i++) {

						if(threads[i].joined == true){
							allClientsJoined = true;
						}
						else{
							allClientsJoined = false;
						}
				}

			}
			
			// build player and player settings for each client
			for (int i = 0; i < count; i++) {
				intergroup.board.Board.Player player = ProtocolBuilder.buildPlayer(threads[i].id);
				threads[i].player = player;
				//System.out.println(player);
				
				intergroup.lobby.Lobby.GameSetup.PlayerSetting playerSetting= ProtocolBuilder.buildPlayerSetup(threads[i].username, threads[i].player, threads[i].id);
				threads[i].players.add(playerSetting);
			}


//			intergroup.board.Board.Player player = ProtocolBuilder.buildPlayer(this.id);
//			this.player = player;
//			
//			intergroup.lobby.Lobby.GameSetup.PlayerSetting playerSetting= ProtocolBuilder.buildPlayerSetup(this.username, player, this.id);
//			//System.out.println(playerSetting.toString());
//			if(players ==null){
//				System.out.println("err");
//			}
//			players.add(playerSetting);
			System.out.println(players.toString());

			//ask cnb for game setup
			RequestOuterClass.GameStartRequest gameStartReq = ProtocolBuilder.buildGameStartRequest(players, 0);
			RequestOuterClass.GameControlRequest gameContReq = ProtocolBuilder.buildGameControlRequest(gameStartReq);
			RequestOuterClass.Request req = RequestOuterClass.Request.newBuilder().setGameControlRequest(gameContReq).build();
			zOut.sendRequest(req);

			intergroup.lobby.Lobby.GameSetup gameSetup = null;
			// loop until cnb has sent game setup
			boolean received = false;
			System.out.println("Waiting for game setup...");
			while(received == false){
				System.out.print(".");
				intergroup.Messages.Message msg = zIn.getMsg();
				if(msg!= null){
					// game setup should sent via zqm to zmqin, and then sent over is to here

					gameSetup = Parser.parseGameSetupMsg(msg);

					if(gameSetup != null){
						received = true;
						System.out.println();
						System.out.println("game setup received!");
						//System.out.println(gameSetup.toString());

					}
				}
			}

			System.out.println(count);
			// deconstruct game setup msg from cnb, add in ownPlayer for each client
			for (int i = 0; i < count; i++) {
				if (threads[i] != null) {
					//System.out.println(threads[i].player.toString());
					//gameSetup.toBuilder().setOwnPlayer(threads[i].player).build();

					intergroup.lobby.Lobby.GameSetup setup = intergroup.lobby.Lobby.GameSetup
							.newBuilder()
							.addAllHexes(gameSetup.getHexesList())
							.addAllHarbours(gameSetup.getHarboursList())
							.addAllPlayerSettings(players)
							.setOwnPlayer(threads[i].player)
							.build();

					// build game state and send this to all clients
					intergroup.Messages.Message msg = ProtocolBuilder.buildBeginGame(setup);
					//System.out.println(setup.toString());
					System.out.println("Start Game Event sent to Client " + i);
					msg.writeDelimitedTo(threads[i].os);
				}
				else {
					System.out.println("Thread " + i + " not connected");

				}
			}
			
			// start game!?!
			
			


			// main while loop to take input and relay to all clients
			while (true) {


				intergroup.Messages.Message msg2 = intergroup.Messages.Message.parseDelimitedFrom(is);

				// parse messages from output stream
				System.out.println("Main Loop:" + msg2.toString());


				// relay input received to all clients
				for (int i = 0; i < maxClientsCount; i++) {
					if (threads[i] != null) {
						msg2.writeDelimitedTo(threads[i].os);
					}
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			//clean up and set current thread as null so another client can be accepted
			for (int i = 0; i < maxClientsCount; i++) {
				if (threads[i] == this) {
					threads[i] = null;
				}
			}

			// close i/o streams and sockets
			try {
				is.close();
				os.close();
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}


	public boolean sendRequest(RequestOuterClass.Request request){
		//System.out.println(request);
		ZMQ.Socket socket = Server.socket;
		socket.sendMore(RequestOuterClass.Request.getDescriptor().getFullName());
		socket.send(request.toByteArray(), 0);

		try {
			String responseType = socket.recvStr();
			if(!socket.hasReceiveMore()){
				System.err.println("Invalid packet received");
			}

			byte[] reply = socket.recv();
			bleat.ResponseOuterClass.Response response = bleat.ResponseOuterClass.Response.parseFrom(reply);
			System.out.println("Success: " + response.getSuccess());
			//            return response.getSuccess();
			return true;
		} catch ( InvalidProtocolBufferException x){
			System.err.println(x);
		}

		return false;
	}

}