import bleat.BleatEvents;
import bleat.BleatEvents.BleatEvent;
import bleat.RequestOuterClass;
import intergroup.Events;
import intergroup.Requests;
import intergroup.board.Board.Edge;
import intergroup.lobby.Lobby;
import intergroup.lobby.Lobby.GameSetup;
import intergroup.lobby.Lobby.Join;

public class Parser {
	
	 public static Join parseJoinMsg(intergroup.Messages.Message msg){
	        switch(msg.getTypeCase()){
	        case EVENT:
	        	Events.Event event = msg.getEvent();
	        	switch(event.getTypeCase()){
	        	case ROADBUILT:

	        		break;
	        	case SETTLEMENTBUILT:

	        		break;
	        	case CITYBUILT:

	        		break;
	        	case TURNENDED:

	        		break;
	        	case ROLLED:

	        		break;
	        	}
	        	break;
	        case REQUEST:
	        	Requests.Request request = msg.getRequest();
	        	switch(request.getBodyCase()){
	        	case JOINLOBBY:
	        		Join join = request.getJoinLobby();
	        		return join;
	        		
				default:
					return null;
	        		
	        	}
			default:
	            	return null;
	        }
			return null;
	    }

	public static GameSetup parseGameSetupMsg(intergroup.Messages.Message msg) {
		switch(msg.getTypeCase()){
		case EVENT:
        	intergroup.Events.Event event = msg.getEvent();	
        	intergroup.lobby.Lobby.GameSetup gameSetup = event.getBeginGame();
        	return gameSetup;
		default: return null;
        }
	}

//	public static intergroup.lobby.Lobby.GameSetup parseGameSetupMsg(byte[] contents) {
//		// TODO Auto-generated method stub
//		
//	}
}
