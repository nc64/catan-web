

import CatanMain.CoordRotation;

public class Edge {
    
    private Coordinate pair1;
    private Coordinate pair2;
    
    private CoordRotation newPair;
    
    public Edge(int x1, int y1, int x2, int y2){
        pair1 = new Coordinate(x1, y1);
        pair2 = new Coordinate(x2, y2);
    }

    public Coordinate getPair1() {
        return pair1;
    }

    public void setPair1(Coordinate pair1) {
        this.pair1 = pair1;
    }

    public Coordinate getPair2() {
        return pair2;
    }

    public void setPair2(Coordinate pair2) {
        this.pair2 = pair2;
    }

    public CoordRotation getNewPair() {
        return newPair;
    }

    public void setNewPair(CoordRotation newPair) {
        this.newPair = newPair;
    }
    
    public class Coordinate {
        private int x;
        private int y;

        public Coordinate(int x, int y){
            this.x = x;
            this.y = y;
        }
        
        
        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
        
        
    }
    
}


