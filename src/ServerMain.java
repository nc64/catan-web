import java.net.UnknownHostException;

public class ServerMain {

	public static void main(String[] args) throws UnknownHostException {
		
		// start server
		Server server = new Server();
		Server.start();
	}

}
