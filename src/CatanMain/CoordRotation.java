package CatanMain;

public class CoordRotation{

    public int x;
    public int y;
    public int rot;
    
    public CoordRotation(int x, int y, int rot) {
        this.x = x;
        this.y = y;
        this.rot = rot;
    }
    
    public CoordRotation(int x1, int y1, int x2, int y2){
        System.out.println("MID X1: " + x1 + " Y1 " + y1 + " X2 " + x2 + " y2 " + y2);
        int originX;
        int originY;
        int otherX;
        int otherY;
        
        int sumXY1 = x1 + y1;
        if (sumXY1 == 1 || sumXY1 == 4 || sumXY1 == -2 || sumXY1 == -5 || sumXY1 == 7 || sumXY1 == -8) {
            originX = x1;
            originY = y1;
            otherX = x2;
            otherY = y2;

        } else {
            originX = x2;
            originY = y2;
            otherX = x1;
            otherY = y1;
        }
        this.x = originX;
        this.y = originY;
        if(otherX - originX == 0){
            this.rot = 0;
        } else {
            if(otherX - originX < 0){
                this.rot = 4;
            }
            else{
                if(otherX - originX > 0){
                    this.rot = 2;
                }
            }
        }
        System.out.println("NEW X: " + this.x + " , Y : "+ this.y + " , R : " + this.rot);
        
    }

    @Override
    public int hashCode() {
        Integer xObj = x;
        Integer yObj = new Integer(y);
        Integer rotObj = new Integer(rot);

        return xObj.hashCode()+yObj.hashCode()+rotObj.hashCode();
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != CoordRotation.class){
            return false;
        }
        CoordRotation objNew = (CoordRotation) obj;
        if((this.x == objNew.x) && (this.y == objNew.y) && (this.rot == objNew.rot)){
            return true;
        }
        else{
            return false;
        }
    }

}
