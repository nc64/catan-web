import intergroup.Requests;
import intergroup.board.Board;
import intergroup.board.Board.Player;
//import intergroup.board.Board.Edge;
import intergroup.board.Board.Point;
import intergroup.lobby.Lobby.GameSetup;
import intergroup.lobby.Lobby.GameSetup.PlayerSetting;
import intergroup.lobby.Lobby.GameSetup.PlayerSetting.Colour;

import java.util.ArrayList;

import com.google.protobuf.Value;

import bleat.RequestOuterClass;
import bleat.RequestOuterClass.GameControlRequest;
import bleat.RequestOuterClass.GameStateRequest;

public class ProtocolBuilder {
    
    public static intergroup.board.Board.Point buildPoint(int x, int y){
        return intergroup.board.Board.Point.newBuilder().setX(x).setY(y).build();
    }
    
    public static intergroup.board.Board.Edge buildEdge(int x1, int y1, int x2, int y2){
        return intergroup.board.Board.Edge.newBuilder().setA(buildPoint(x1, y1)).setB(buildPoint(x2, y2)).build();
    }
    
    public static intergroup.board.Board.Edge edgeFromRoadBuild(bleat.BleatEvents.OnRoadBuiltEvent event){
        intergroup.board.Board.Edge edge = event.getEdge();
        intergroup.board.Board.Point point1 = edge.getA();
        intergroup.board.Board.Point point2 = edge.getB();
        return edge;
    }
    
    public static intergroup.board.Board.Player buildPlayer(int id){
    	return intergroup.board.Board.Player.newBuilder()
    		.setIdValue(id)
    		.build();
    }
    
  public static intergroup.lobby.Lobby.GameSetup.PlayerSetting buildPlayerSetup(String username, Player player, int colour){
	  
	  return intergroup.lobby.Lobby.GameSetup.PlayerSetting.newBuilder()
			  .setUsername(username)
			  .setPlayer(player)
			  .setColourValue(colour)
			  .build();
}

public static RequestOuterClass.GameStartRequest buildGameStartRequest(ArrayList<GameSetup.PlayerSetting> players, int boardType) {
	
	return bleat.RequestOuterClass.GameStartRequest.newBuilder()
			.addAllPlayerSettings(players)
			.setBoardTypeValue(boardType)
			.build();
}

public static GameControlRequest buildGameControlRequest(RequestOuterClass.GameStartRequest req) {
	
	return bleat.RequestOuterClass.GameControlRequest.newBuilder()
			.setGameStartRequest(req)
			.build();
}

public static intergroup.Messages.Message buildBeginGame(GameSetup gameSetup) {
	
	intergroup.Events.Event event = intergroup.Events.Event.newBuilder().setBeginGame(gameSetup).build();
    intergroup.Messages.Message msg = intergroup.Messages.Message.newBuilder().setEvent(event).build();
    // System.out.println("PROTOBUILDER: "+msg.toString());
    return msg;
	
}



  
//  public static intergroup.lobby.Lobby.GameSetup buildGameSetup(Board.Hex hexes, Board.Harbour harbours, PlayerSetting playerSettings, Player player){
//	  
//	  return intergroup.lobby.Lobby.GameSetup.newBuilder()
//			  .setOwnPlayer(player)
//			  .setHarbours(harbours, value)
//			  .setPlayerSettings(playerSettings)
//			  .build();
//}
    
        
}
